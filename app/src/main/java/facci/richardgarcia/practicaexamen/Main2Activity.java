package facci.richardgarcia.practicaexamen;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class Main2Activity extends AppCompatActivity {
    EditText  editTextIngresoCodigo, editTextIngresoPlaca, editTextIngresoFecha, editTextIngresoHora, editTextIngresoPlaza;
    Button buttonIngresoDatos ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        editTextIngresoCodigo = (EditText) findViewById(R.id.editTextIngresoCodigo);
        editTextIngresoPlaca = (EditText) findViewById(R.id.editTextIngresoPlaca);
        editTextIngresoFecha = (EditText) findViewById(R.id.editTextIngresoFecha);
        editTextIngresoHora = (EditText) findViewById(R.id.editTextIngresoHora);
        editTextIngresoPlaza = (EditText) findViewById(R.id.editTextIngresoPlaza);
        buttonIngresoDatos = (Button) findViewById(R.id.buttonIngresoDatos);
        buttonIngresoDatos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Ingresar(v);
            }
        });
    }
    public void Ingresar (View view){
        DBHelper admin= new DBHelper(this);
        SQLiteDatabase db = admin.getWritableDatabase();
        String codigo = editTextIngresoCodigo.getText().toString();
        String placa = editTextIngresoPlaca.getText().toString();
        String fecha = editTextIngresoFecha.getText().toString();
        String hora = editTextIngresoHora.getText().toString();
        String plaza = editTextIngresoPlaza.getText().toString();
        if (!codigo.isEmpty()&& !placa.isEmpty() &&!fecha.isEmpty() && !hora.isEmpty() && !plaza.isEmpty()){
            ContentValues registro = new ContentValues();
            registro.put(DBManager.CODIGO, codigo);
            registro.put(DBManager.PLACA, placa);
            registro.put(DBManager.FECHA, fecha);
            registro.put(DBManager.HORA, hora);
            registro.put(DBManager.PLAZA, plaza);
            db.insert(DBManager.TABLE_NAME, null, registro);
            db.close();
            editTextIngresoCodigo.setText("");
            editTextIngresoPlaca.setText("");
            editTextIngresoFecha.setText("");
            editTextIngresoHora.setText("");
            editTextIngresoPlaza.setText("");
            Toast.makeText(this, "Ingreso exitoso", Toast.LENGTH_LONG).show();
            finish();
        }else{
            Toast.makeText(this, "Debe llenar todos los campos", Toast.LENGTH_LONG).show();
        }
    }
}
