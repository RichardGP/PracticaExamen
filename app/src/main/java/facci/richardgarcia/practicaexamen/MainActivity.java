package facci.richardgarcia.practicaexamen;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText editTextConsulta;
    Button buttonConsulta;
    TextView textViewCodigo, textViewPlaca, textViewFecha, textViewHora, textViewPlaza;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editTextConsulta = (EditText) findViewById(R.id.editTextConsulta);
        textViewCodigo = (TextView) findViewById(R.id.textViewCodigo);
        textViewPlaca = (TextView) findViewById(R.id.textViewPlaca);
        textViewFecha = (TextView)findViewById(R.id.textViewFecha);
        textViewHora = (TextView) findViewById(R.id.textViewHora);
        textViewPlaza = (TextView) findViewById(R.id.textViewPlaza);

        buttonConsulta = (Button) findViewById(R.id.buttonConsulta);
        buttonConsulta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Consultar(v);
            }
        });

    }



    public void Consultar(View view){
        DBHelper admin = new DBHelper(this);
        SQLiteDatabase db = admin.getReadableDatabase();
        String codigo = editTextConsulta.getText().toString();
        if (!codigo.isEmpty()){
            Cursor fila = db.rawQuery
                    ("select * from "+DBManager.TABLE_NAME+" where codigo =" + codigo , null);
            if (fila.moveToFirst()){
                textViewCodigo.setText(fila.getString(0));
                textViewPlaca.setText(fila.getString(1));
                textViewFecha.setText(fila.getString(2));
                textViewHora.setText(fila.getString(3));
                textViewPlaza.setText(fila.getString(4));
                db.close();
                Toast.makeText(this, "El codigo existe", Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(this, "El codigo no existe, Ingreselo", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(MainActivity.this, Main2Activity.class);
                db.close();
                startActivity(intent);
            }

        }
    }
}
